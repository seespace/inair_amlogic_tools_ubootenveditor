ifneq ($(TARGET_SIMULATOR),true)

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_SRC_FILES:= fw_env_main.c fw_env.c
LOCAL_C_INCLUDES += external/zlib
LOCAL_MODULE := fw_printenv
LOCAL_MODULE_TAGS := optional
LOCAL_STATIC_LIBRARIES := libcutils libc libz
LOCAL_FORCE_STATIC_EXECUTABLE := true
ifeq ($(TARGET_BOARD_PLATFORM), meson8)
LOCAL_CFLAGS += -DMESON8_ENVSIZE
endif
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_SRC_FILES:= fw_env_main.c fw_env.c
LOCAL_C_INCLUDES += external/zlib
LOCAL_MODULE := libfw_env
LOCAL_MODULE_TAGS := optional
LOCAL_STATIC_LIBRARIES := libcutils libc libz
LOCAL_FORCE_STATIC_EXECUTABLE := true
ifeq ($(TARGET_BOARD_PLATFORM), meson8)
LOCAL_CFLAGS += -DMESON8_ENVSIZE
endif
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_SRC_FILES:= fw_env_main.c fw_env.c
LOCAL_C_INCLUDES += external/zlib
LOCAL_MODULE := fw_setenv
LOCAL_MODULE_TAGS := optional
LOCAL_STATIC_LIBRARIES := libcutils libc libz
LOCAL_FORCE_STATIC_EXECUTABLE := true
ifeq ($(TARGET_BOARD_PLATFORM), meson8)
LOCAL_CFLAGS += -DMESON8_ENVSIZE
endif
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_SRC_FILES:= fw_env_main.c fw_env.c
LOCAL_C_INCLUDES += external/zlib
LOCAL_MODULE := sbin/fw_printenv
LOCAL_MODULE_PATH := $(TARGET_RECOVERY_ROOT_OUT)
LOCAL_MODULE_TAGS := optional
LOCAL_STATIC_LIBRARIES := libcutils libc libz
LOCAL_FORCE_STATIC_EXECUTABLE := true
ifeq ($(TARGET_BOARD_PLATFORM), meson8)
LOCAL_CFLAGS += -DMESON8_ENVSIZE
endif
include $(BUILD_EXECUTABLE)
	
include $(CLEAR_VARS)
LOCAL_SRC_FILES:= fw_env_main.c fw_env.c
LOCAL_C_INCLUDES += external/zlib
LOCAL_MODULE := sbin/fw_setenv
LOCAL_MODULE_PATH := $(TARGET_RECOVERY_ROOT_OUT)
LOCAL_MODULE_TAGS := optional
LOCAL_STATIC_LIBRARIES := libcutils libc libz
LOCAL_FORCE_STATIC_EXECUTABLE := true
ifeq ($(TARGET_BOARD_PLATFORM), meson8)
LOCAL_CFLAGS += -DMESON8_ENVSIZE
endif
include $(BUILD_EXECUTABLE)

endif  # TARGET_SIMULATOR != true
